package fr.ulille.iutinfo.teletp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

private String salle;
private String poste;
private String DISTANCIEL[] ;
private SuiviViewModel mod;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    @SuppressLint("ResourceType")
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    DISTANCIEL=  getResources().getStringArray(R.array.list_salles)   ;
    poste = "";
    salle = DISTANCIEL[0];
        mod = new SuiviViewModel(getActivity().getApplication());
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter arrayAdapterSalle = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles, android.R.layout.simple_spinner_item);
        ArrayAdapter arrayAdapterPostes = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes, android.R.layout.simple_spinner_item);
        spSalle.setAdapter(arrayAdapterSalle);
        spPoste.setAdapter(arrayAdapterPostes);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
        TextView tvLogin = (TextView) view.findViewById(R.id.tvLogin);
        mod.setUsername(tvLogin.getText().toString());
        NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        // TODO Q9
    }
       public void update(){
           Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
           Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
           if (spSalle.getSelectedItem().toString().equals("DISTANCIEL")){
               spPoste.setVisibility(View.GONE);
           }
       }
    // TODO Q9
}